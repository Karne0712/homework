from random import randint

number = randint(0, 10)
guess = None

while guess != number:
    guess = int(input('Գուշակի թիվը 0-10 միջավայրում\n'))
    if guess > 10 or guess < 0:
       print('Մուտքագրած թիվը  0-10 միջավայրում չէ\n')
    elif guess > number:
       print('Ձեր մուտքագրած թիվը մեծ է\n')
    else:
       print('Ձեր մուտքագրած թիվը փոքր է\n')

print('Շնորհավորում եմ դուք ճիշտ գուշակեցիք: Ու շահել եք միացրած ֆեն\n')

